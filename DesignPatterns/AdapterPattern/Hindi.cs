﻿using System;

namespace AdapterPattern
{
    public class Hindi
    {
        public void Greet()
        {
            Console.WriteLine("Namaste");
        }

        public void Bid()
        {
            Console.WriteLine("Phir milte hain");
        }
    }
}
