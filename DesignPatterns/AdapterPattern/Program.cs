﻿using System;
/*
 *  ADAPTER PATTERN is a software design pattern that allows the interface of an existing class to be used from another interface.
 *  It is often used to make existing classes work with others without modifying their source code.
 */
namespace AdapterPattern
{
    class Program
    {
        static void Main()
        {
            var translator = new Translator();
            Console.WriteLine("Enter a word to be transalated to Hindi:");
            var sentence = Console.ReadLine();
            translator.Translate(sentence);
            Console.ReadKey();
        }
    }
}
