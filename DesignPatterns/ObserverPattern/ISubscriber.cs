﻿

namespace ObserverPattern
{
    public interface ISubscriber
    {
        void Notify(string username);
    }
}
