﻿
namespace DecoratorPattern.Component
{
    public abstract class IceCream
    {
        public abstract double Cost();
    }
}
