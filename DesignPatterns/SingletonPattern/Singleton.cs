﻿
using System;

namespace SingletonPattern
{
    public class Singleton
    {
        private static Singleton _singleton;

        private Singleton() {}

        public static Singleton GetInstance()
        {
            return _singleton ?? (_singleton = new Singleton());
        }

        public void Login()
        {
            Console.WriteLine("Login successful!");
        }

    }
}
