﻿namespace StrategyPattern
{
    public interface IWeapon
    {
        void Use();
    }
}
