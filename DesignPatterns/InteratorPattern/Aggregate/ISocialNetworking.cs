﻿
using InteratorPattern.Iterator;

namespace InteratorPattern.Aggregate
{
    public interface ISocialNetworking
    {
        IIterator CreateIterator();
    }
}
