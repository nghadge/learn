﻿namespace NullPattern
{
    public interface IWeapon
    {
        void Use();
    }
}
